/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package componentes.jtable.renderer;

/**
 *
 * @author JADPA29
 */
public class Vehiculo {
    private String marca;
    private String modelo;
    private String motor;
    private String desplazamiento;
    private String velocidad;
    private String cilindros;
    private String imageURL;

    public Vehiculo() {
    }

    public Vehiculo(String marca, String modelo, String motor, String desplazamiento, String velocidad) {
        this.marca = marca;
        this.modelo = modelo;
        this.motor = motor;
        this.desplazamiento = desplazamiento;
        this.velocidad = velocidad;
    }    

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMotor() {
        return motor;
    }

    public void setMotor(String motor) {
        this.motor = motor;
    }

    public String getDesplazamiento() {
        return desplazamiento;
    }

    public void setDesplazamiento(String desplazamiento) {
        this.desplazamiento = desplazamiento;
    }

    public String getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(String velocidad) {
        this.velocidad = velocidad;
    }

    public String getCilindros() {
        return cilindros;
    }

    public void setCilindros(String cilindros) {
        this.cilindros = cilindros;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
}
